---
layout: event_detail
title: Mapping the Big Picture - Projects
event: hamburg2023
order: 25
permalink: /events/hamburg2023/projects/
---

Projects we want/need to be reproducible:
    
- binutils
- PureOS/Mobian; RB images for phones
- filesystems
- npm
- Rust crates
- PyPI
- Docker directory timestamps
- ElectroBSD/FreeBSD Ports/Packages
- Qt
- Python Sphinx
- ar embeds mtime, uid, gid
- Gradle
- dpkg database
- RB in Ubuntu
- Compiler for embed, Aurix (Infineon), ARM/Keil
- Gcc
- Python 3.9
- Cloiure, build-tools dependencies
- Flatpack
- Docker images
