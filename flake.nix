{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    systems.url = "github:nix-systems/default";
  };

  outputs = inputs @ { self, flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
    systems = import inputs.systems;

    perSystem = { config, self', inputs', pkgs, system, lib, ... }:
      let
        jekyll = pkgs.jekyll.override ({ withOptionalDependencies = true; });
        ruby = (pkgs.ruby.withPackages (ps: [ jekyll ]));
        python3 = pkgs.python3.withPackages (p: [ p.pyyaml ]);
      in
      {
        packages = {
          default = pkgs.stdenv.mkDerivation {
            name = "reproducible-website";
            src = ./.;

            # This step should not be mandatory, the script contributors.py
            # should not get the contributors from git.
            # Without this preConfigure phase, the script will fail.
            preConfigure = ''
              git init
              git config user.email "you@example.com"
              git config user.name "Nix build"

              git add .
              git commit -am "nix build commit"
            '';

            postPatch = ''
              substituteInPlace bin/i18n.sh \
                --replace "#!/bin/bash" "#!${pkgs.runtimeShell}"
              substituteInPlace bin/contributors.py \
                --replace "#!/usr/bin/env python3" "#!${lib.getExe python3}"
                cat bin/contributors.py
            '';

            buildInputs = [
              pkgs.git
              pkgs.perl536Packages.Po4a
              pkgs.which
              python3
              ruby
            ];

            installPhase = ''
              runHook preInstall

              mkdir -p $out
              cp -ar _site/* $out/

              runHook postInstall
            '';
          };
        };

        apps = {
          default =
            let
              caddyFile = pkgs.writeText "Caddyfile" ''
                :4000 {
                  root * ${self'.packages.default}
                  log
                  encode gzip
                  file_server
                }
              '';
            in
            {
              type = "app";
              program = (lib.getExe (pkgs.writeScriptBin "start-static-site" ''
                #!${pkgs.runtimeShell}
                ${lib.getExe pkgs.caddy} run --adapter caddyfile --config ${caddyFile}
              ''));
            };

          devserver = {
            type = "app";
            program = (lib.getExe (pkgs.writeScriptBin "start-static-server" ''
              #!${pkgs.runtimeShell}
              ${lib.getExe jekyll} serve --host
            ''));
          };

          serve = {
            type = "app";
            program = (lib.getExe (pkgs.writeScriptBin "start-live-reload-site" ''
              #!${pkgs.runtimeShell}
              ${lib.getExe jekyll} serve --watch --livereload --incremental --host
            ''));
          };
        };

        devShells = {
          default = pkgs.stdenvNoCC.mkDerivation {
            name = "devshell";
            buildInputs = [
              pkgs.pandoc
              pkgs.gnumake
              pkgs.perl536Packages.Po4a
              pkgs.which
              jekyll
              python3
              ruby
            ];
          };
        };
      };
  };
}
